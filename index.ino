#include "FastLED.h"

#define LED_PIN 4
#define LED_COUNT 90
#define COLOR_COUNT 4

volatile unsigned long index = 0;

CRGB leds[LED_COUNT];

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, LED_COUNT);
  FastLED.setBrightness(50);
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);

  if (ledIndex % 2 == 0) {
    leds[ledIndex] = CRGB::Red;
  } else {
    leds[ledIndex] = CRGB::Orange;
  }
  
  if (ledIndex == LED_COUNT - 1) {
    ledIndex = 0;
  } else {
    ledIndex++;
  }
  
  FastLED.show();
  digitalWrite(LED_BUILTIN, LOW);
  delay(10);
}
