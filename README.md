# Carpenter Brut Tee

## Configuring

- Download the [Arduino IDE](https://www.arduino.cc/en/Main/Software) (for desktop)
- Install the Visual Studio Code [Arduino extension](https://github.com/Microsoft/vscode-arduino#installation)
- Use *Arduino: Library Manager* to install `FastLED`
- Use *Arduino: Change Board Type* to select the board used (e.g.: *Arduino Pro or Pro Mini*)
- Use *Arduino: Select Serial Port* to select the correct serial port

## Uploading

- Use *Arduino: Upload*
